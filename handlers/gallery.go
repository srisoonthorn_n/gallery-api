package handlers //ชื่อควรตรงกับ Folder

import (
	"fmt"
	"gallery-api/context"
	"gallery-api/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Gallery struct {
	ID            uint       `json:"id"`
	Name          string     `json:"name"`
	StatusPublish bool       `json:"status_publish"`
	Images        []ImageRes `json:"images"`
}

type GalleryHandler struct {
	// db *gorm.DB
	gs models.GalleryService
}

func NewGalleryHandler(gs models.GalleryService) *GalleryHandler {
	return &GalleryHandler{gs}

}

type CreateGallery struct {
	// gorm.Model
	Name string `json:"name"`
}

type CreateRes struct {
	Gallery
}

//--------------------Create Gallery ---------------------

func (gh *GalleryHandler) CreateGallerys(c *gin.Context) {
	user := context.User(c)
	fmt.Println("Showwww user", user)
	if user == nil {
		c.Status(401)
		return
	}

	data := new(CreateGallery)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	gallery := new(models.Gallery)
	gallery.Name = data.Name
	gallery.UserID = user.ID
	if err := gh.gs.CreateGallerys(gallery); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(Gallery)
	res.ID = gallery.ID
	res.Name = gallery.Name
	res.StatusPublish = gallery.StatusPublish
	c.JSON(201, gin.H{
		"id":             gallery.ID,
		"name":           gallery.Name,
		"status_publish": gallery.StatusPublish,
	})

}

type NewGallery struct {
	ID            uint       `json:"id"`
	Name          string     `json:"name"`
	StatusPublish bool       `json:"status_publish"`
	Images        []ImageRes `json:"images"`
}

//--------------------List All Publish ---------------------
func (gh *GalleryHandler) ListAllPublish(c *gin.Context) {
	galliers, err := gh.gs.ListAllPublish()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	newGallery := []NewGallery{}
	for _, d := range galliers {
		images := []ImageRes{}
		for _, img := range d.Images {
			images = append(images, ImageRes{
				ID:        img.ID,
				GalleryID: img.GalleryID,
				Filename:  img.URLPath(),
			})
		}
		newGallery = append(newGallery, NewGallery{
			ID:            d.ID,
			Name:          d.Name,
			StatusPublish: d.StatusPublish,
			Images:        images,
		})
	}

	c.JSON(200, newGallery)
}

//--------------------Delete Gallery ---------------------

func (gh *GalleryHandler) DeleteGallery(c *gin.Context) {
	idSrting := c.Param("id") //ต้องเปลี่ยน id ให้ตรงกับ main.go
	ID, err := strconv.Atoi(idSrting)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	if err := gh.gs.DeleteGallery(uint(ID)); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}

//--------------------List Gallery ----------------------

func (gh *GalleryHandler) ListGallery(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	data, err := gh.gs.ListByUserID(user.ID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	galleries := []Gallery{}
	for _, d := range data {
		galleries = append(galleries, Gallery{
			ID:            d.ID,
			Name:          d.Name,
			StatusPublish: d.StatusPublish,
		})
	}
	c.JSON(200, galleries)
}

//-------------------Get One -------------------------
func (gh *GalleryHandler) GetOne(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err := gh.gs.GetByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, Gallery{
		ID:            data.ID,
		Name:          data.Name,
		StatusPublish: data.StatusPublish,
	})
}

//----------------------Update Name ---------------------

type UpdateNameReq struct {
	Name string `json:"name"`
}

func (gh *GalleryHandler) UpdateName(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	req := new(UpdateNameReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	err = gh.gs.UpdateGalleryName(uint(id), req.Name)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}

//------------------------------Update Status------------------------

type UpdateStatusReq struct {
	StatusPublish bool `json:"status_publish"`
}

func (gh *GalleryHandler) UpdateStatus(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	req := new(UpdateStatusReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	err = gh.gs.UpdateGalleryStatus(uint(id), req.StatusPublish)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}

//-------------------------------- List from Gallery on Pub-----------------

func (gh *GalleryHandler) GetImagesPublish(c *gin.Context) {

	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err := gh.gs.GetImagesByGalleryID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)

}
