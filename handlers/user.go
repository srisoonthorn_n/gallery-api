package handlers

import (
	"gallery-api/models"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
	us models.UserService
	// user *models.User
}

type SignupReq struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func NewUserHandler(us models.UserService) *UserHandler {
	return &UserHandler{us}
}

func (uh *UserHandler) Signup(c *gin.Context) {
	req := new(SignupReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	user := new(models.User)
	user.Email = req.Email
	user.Password = req.Password

	if err := uh.us.Create(user); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"token": user.Token,
		"email": user.Email,
	})
}

type LoginReq struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	// Token    string `gorm:"unique_index"`
}

func (uh *UserHandler) Login(c *gin.Context) {
	req := new(LoginReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"meesage": err.Error(),
		})
		return
	}
	user := new(models.User)
	user.Email = req.Email
	user.Password = req.Password
	token, err := uh.us.Login(user)
	if err != nil {
		c.JSON(401, gin.H{
			"meesage": err.Error(),
		})
		return
	}
	// c.JSON(201, gin.H{
	// 	"message": "Login Success",
	// })
	// return
	c.JSON(201, gin.H{
		"token": token,
		"email": user.Email,
	})
}

//CSRG Cross-site request forgery
