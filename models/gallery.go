package models

import (
	"log"
	"os"
	"path/filepath"
	"strconv"

	"github.com/jinzhu/gorm"
)

type Gallery struct {
	gorm.Model
	Name          string `gorm:"column:name"`
	StatusPublish bool   `gorm:"column:status_publish"`
	Images        []Image
	UserID        uint
}

type GalleryService interface {
	CreateGallerys(gallery *Gallery) error //ใช้ interface เพื่อให้ใช้แทนกันได่้ หน้าตาต้องเหมือนกับ (2)
	ListAllPublish() ([]Gallery, error)
	DeleteGallery(ID uint) error
	GetByID(id uint) (*Gallery, error)
	ListByUserID(id uint) ([]Gallery, error)
	UpdateGalleryName(id uint, name string) error
	UpdateGalleryStatus(id uint, status_publish bool) error
	GetImagesByGalleryID(id uint) ([]Image, error)
}

func NewGalleryService(db *gorm.DB) GalleryService {
	return &galleryGorm{db}
}

type galleryGorm struct {
	db *gorm.DB
}

//--------------------------CreateGallerys---------------------------------------

func (gg *galleryGorm) CreateGallerys(gallery *Gallery) error { //(2)
	return gg.db.Create(gallery).Error
}

//--------------------------List All Gallery Publish---------------------------------------

func (gg *galleryGorm) ListAllPublish() ([]Gallery, error) {
	galleries := []Gallery{}
	err := gg.db.
		Where("status_publish = ?", true).
		Find(&galleries).Error
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(galleries); i++ {
		images := []Image{}
		err := gg.db.
			Where("id = ?", galleries[i].ID).
			Find(&images).Error
		if err != nil {
			return nil, err
		}
		galleries[i].Images = images
	}
	return galleries, nil
}

//--------------------------DeleteGallery---------------------------------------

func (gg *galleryGorm) DeleteGallery(ID uint) error {
	//&Gallery ไม่มี Data แต่บอก gorm เฉยๆว่าจะลบ table ไหน ต้องเป็นอันเดียวกับที่ใช้สร้าง table เหมือน Delete from table .... where
	tx := gg.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		return err
	}
	err := gg.db.Where("id =?", ID).Delete(&Gallery{}).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	idStr := strconv.FormatUint(uint64(ID), 10)
	if err := os.RemoveAll(filepath.Join(UploadPath, idStr)); err != nil {
		log.Printf("Fail deleting image files: %v\n", err)
	}
	return tx.Commit().Error
}

//--------------------------GetByID---------------------------------------

func (gg *galleryGorm) GetByID(id uint) (*Gallery, error) {
	gallery := new(Gallery)
	if err := gg.db.First(gallery, id).Error; err != nil {
		return nil, err
	}
	return gallery, nil
}

//--------------------------ListByUserID---------------------------------------

func (gg *galleryGorm) ListByUserID(id uint) ([]Gallery, error) {
	galleries := []Gallery{}
	if err := gg.db.
		Where("user_id = ?", id).
		Find(&galleries).Error; err != nil {
		return nil, err
	}
	return galleries, nil
}

//--------------------Update Name -----------------------------------------------

func (gg *galleryGorm) UpdateGalleryName(id uint, name string) error {
	return gg.db.Model(&Gallery{}).Where("id =?", id).Update("name", name).Error
}

//--------------------Update Status ------------------------------------------

func (gg *galleryGorm) UpdateGalleryStatus(id uint, status_publish bool) error {
	return gg.db.Model(&Gallery{}).Where("id =?", id).Update("status_publish", status_publish).Error

}

//------------------- Get Image By ID ----------------------

func (gg *galleryGorm) GetImagesByGalleryID(id uint) ([]Image, error) {
	images := []Image{}
	if err := gg.db.
		Where("gallery_id = ?", id).
		Find(&images).Error; err != nil {
		return nil, err
	}
	return images, nil
}
