package middleW

import (
	"gallery-api/models"
	"strings"

	"github.com/gin-gonic/gin"
)

func RequireUser(us models.UserService) gin.HandlerFunc {
	return func(c *gin.Context) {

		header := c.GetHeader("Authorization")

		header = strings.TrimSpace(header)
		if len(header) <= 7 {
			c.Status(401)
			c.Abort()
			return
		}

		token := header[len("Bearer "):]

		if token == "" {

			c.Status(401)
			c.Abort()
			return
		}
		user, err := us.GetByToken(token)
		if err != nil {
			c.Status(401)
			c.Abort()
			return
		}
		c.Set("user", user)

	}

}
