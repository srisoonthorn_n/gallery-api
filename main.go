package main

import (
	"fmt"
	"gallery-api/config"
	"gallery-api/handlers"
	"gallery-api/hash"
	"gallery-api/middleW"
	"gallery-api/models"
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var db *gorm.DB

func CheckToken() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("Tokennnnnnnnnnnnnnnnnnnnnn")
	}

}

const hmacKey = "secret"

func main() {
	conf := config.Load()

	db, err := gorm.Open(
		"mysql",
		// "root:password@tcp(127.0.0.1:3307)/galleryDB?parseTime=true",
		conf.Connection,
	)

	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()
	db.LogMode(true) //Dev Only

	if err := db.AutoMigrate( //Create table สามารถ เพิ่มขึ้นมาได้เลย ถ้าสร้าง struct ใน models
		&models.Gallery{},
		&models.User{},
		&models.Image{},
	).Error; err != nil {
		log.Fatal(err)
	}

	hmac := hash.NewHMAC(conf.HMACKey) // conf
	gs := models.NewGalleryService(db)
	gh := handlers.NewGalleryHandler(gs)

	ims := models.NewImageService(db)

	us := models.NewUserService(db, hmac)
	imh := handlers.NewImageHandler(gs, ims)
	uh := handlers.NewUserHandler(us)

	r := gin.Default()
	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://localhost:3000"}
	config.AllowHeaders = []string{"Content-Type", "Authorization"}
	r.Use(cors.New(config))
	r.Static("/upload", "./upload")

	r.POST("/signup", uh.Signup)
	r.POST("/login", uh.Login)
	r.GET("/galleries", gh.ListAllPublish)
	r.GET("/galleries/:id/images", gh.GetImagesPublish)
	r.GET("/galleries/:id", gh.GetOne)

	auth := r.Group("/auth")

	auth.Use(middleW.RequireUser(us))
	{

		auth.POST("/galleries", gh.CreateGallerys)
		auth.GET("/galleries", gh.ListGallery)
		auth.DELETE("/galleries/:id", gh.DeleteGallery)

		auth.PATCH("/galleries/:id/names", gh.UpdateName)
		auth.PATCH("/galleries/:id/publishes", gh.UpdateStatus)

		auth.POST("/galleries/:id/images", imh.CreateImage)
		auth.GET("/galleries/:id/images", imh.ListGalleryImages)
		auth.DELETE("/images/:id", imh.DeleteImage)

	}

	r.Run()

}

// {
// data := new(CreateGallery)
// if err := c.BindJSON(data); err != nil {
// 	c.JSON(400, gin.H{
// 		"message": err.Error(),
// 	})
// 	return
// }
// gallery := new(Gallery)
// gallery.Name = data.Name
// if err := db.Create(gallery).Error; err != nil {
// 	c.JSON(500, gin.H{
// 		"message": err.Error(),
// 	})
// 	return
// }
// c.JSON(201, gin.H{
// 	"id":   gallery.ID,
// 	"name": gallery.Name,
// })

// })
