package context

import (
	"gallery-api/models"
	"log"

	"github.com/gin-gonic/gin"
)

const key string = "user"

func SetUser(c *gin.Context, user *models.User) {
	log.Printf("Set User %v", user)
	c.Set(key, user)
	log.Printf("Key %v", key)
}

func User(c *gin.Context) *models.User {
	user, ok := c.Value(key).(*models.User)
	if !ok {
		return nil
	}
	return user
}
