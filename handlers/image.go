package handlers

import (
	"gallery-api/models"
	"net/http"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ImageRes struct {
	ID        uint   `json:"id"`
	GalleryID uint   `json:"gallery_id"`
	Filename  string `json:"filename"`
}

type CreateImageRes struct {
	ImageRes
}

type ImageHandler struct {
	gs  models.GalleryService
	ims models.ImageService
}

func NewImageHandler(gs models.GalleryService, ims models.ImageService) *ImageHandler {
	return &ImageHandler{gs, ims}
}

//----------------------Create Image ---------------------------

func (imh *ImageHandler) CreateImage(c *gin.Context) {
	galleryIDStr := c.Param("id")
	galleryID, err := strconv.Atoi(galleryIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	gallery, err := imh.gs.GetByID(uint(galleryID))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	form, err := c.MultipartForm()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	images, err := imh.ims.CreateImages(form.File["photos"], gallery.ID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := []CreateImageRes{}
	for _, img := range images {
		r := CreateImageRes{}
		r.ID = img.ID
		r.GalleryID = gallery.ID
		r.Filename = filepath.Join(models.UploadPath, galleryIDStr, img.Filename)
		res = append(res, r)
	}
	c.JSON(201, res)
}

//-------------------Delete Image---------------------------

func (imh *ImageHandler) DeleteImage(c *gin.Context) {
	imageIDStr := c.Param("id")
	id, err := strconv.Atoi(imageIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	if err := imh.ims.Delete(uint(id)); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(http.StatusOK)
}

type ListGalleryImagesRes struct {
	ImageRes
}

//----------------------List Gallery Images-----------------------------

func (imh *ImageHandler) ListGalleryImages(c *gin.Context) {
	galleryIDStr := c.Param("id")
	id, err := strconv.Atoi(galleryIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	gallery, err := imh.gs.GetByID(uint(id))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	images, err := imh.ims.GetByGalleryID(gallery.ID)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := []ListGalleryImagesRes{}
	for _, img := range images {
		r := ListGalleryImagesRes{}
		r.ID = img.ID
		r.GalleryID = gallery.ID
		r.Filename = img.FilePath()
		res = append(res, r)
	}
	c.JSON(http.StatusOK, res)
}
